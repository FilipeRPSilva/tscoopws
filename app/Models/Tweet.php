<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    protected $fillable = [
        'text',
        'created_at'
    ];

    public $timestamps = false;

    protected $table = 'tscoop.tweets';

    protected $with = 'keywords';

    public function keywords() {
        return $this->belongsTo(Keyword::class, 'keywords_id', 'id');
    }

    public function sentiment(){
        return $this->hasOne(Sentiment::class, 'tweets_id', 'id');
    }

}
