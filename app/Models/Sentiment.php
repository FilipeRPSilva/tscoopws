<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sentiment extends Model
{
    protected $fillable = [
        'label',
        'score',
        'tweets_id',
        'tools_id'
    ];

    protected $table = 'tscoop.sentiments';
    public $timestamps = false;

    public function tweet() {
        return $this->belongsTo(Tweet::class, 'tweets_id', 'id');
    }

    public function tool(){
        return $this->belongsTo(Tool::class, 'tools_id', 'id');
    }
}
