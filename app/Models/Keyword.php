<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    protected $fillable = [
        'text'
    ];

    protected $table = 'tscoop.keywords';
    public $timestamps = false;

    public function tweets() {
        return $this->hasMany(Tweet::class, 'keywords_id', 'id');
    }
}
