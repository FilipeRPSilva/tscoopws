<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tool extends Model
{
    protected $fillable = [
        'description'
    ];

    protected $table = 'tscoop.tools';
    public $timestamps = false;

}
