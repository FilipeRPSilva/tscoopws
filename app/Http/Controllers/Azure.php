<?php

namespace App\Http\Controllers;


use App\Models\Sentiment;
use App\Models\Tool;

class Azure {

    public $tweet, $options, $url;

    public function __construct($key, $tweet) {

        $this->tweet = $tweet;

        $data = array (
            'documents' => array (
                array ( 'id' => '1',
                    'language' => 'pt',
                    'text' => $this->tweet->text )
            )
        );

        $headers = "Content-type: application/json\r\n" .
            "Ocp-Apim-Subscription-Key: ".$key."\r\n";

        $this->url = 'https://brazilsouth.api.cognitive.microsoft.com/text/analytics/v2.0/sentiment';

        $data = json_encode($data);

        $this->options = array (
            'http' => array (
                'header' => $headers,
                'method' => 'POST',
                'content' => $data
            )
        );
    }

    public function sentiment() {
        $context  = stream_context_create ($this->options);
        $result = file_get_contents ($this->url, false, $context);
        $result = json_decode($result, true);
        $result = $result['documents'];
        $result = $result[0];
        $score = $result['score'];

        $this->storeSentiment($score);
    }

    public function storeSentiment($score){
        $tool = Tool::where('name', 'Microsoft Azure')->first();

        $sentiment = new Sentiment();
        $sentiment->tools_id = $tool->id;
        $sentiment->score = $score;

        if ($sentiment->score <= 0.25) {
            $sentiment->label = 'Negative+';
        }
        if ($sentiment->score > 0.25 && $sentiment->score < 0.5) {
            $sentiment->label = 'Negative';
        }
        if ($sentiment->score >= 0.5 && $sentiment->score < 0.6) {
            $sentiment->label = 'Neutral';
        }
        if ($sentiment->score >= 0.6 && $sentiment->score < 0.75) {
            $sentiment->label = 'Positive';
        }
        if ($sentiment->score >= 0.75) {
            $sentiment->label = 'Positive+';
        }

        $sentiment->tweets_id = $this->tweet->id;
        $sentiment->save();
    }
}
