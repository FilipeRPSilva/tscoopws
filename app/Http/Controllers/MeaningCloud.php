<?php

namespace App\Http\Controllers;


use App\Models\Sentiment;
use App\Models\Tool;

class MeaningCloud
{
    public $key;
    public $tweet;
    public $curl;

    public function __construct($key, $tweet){
        $this->tweet = $tweet;
        $this->key = $key;
        $this->curl = curl_init();

        curl_setopt_array($this->curl, array(
            CURLOPT_URL => "https://api.meaningcloud.com/sentiment-2.1",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "key=".$this->key."&lang=pt&txt=".$this->tweet->text,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded"
            ),
        ));
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, 0);

    }

    public function sentiment(){
        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);
        curl_close($this->curl);
        if ($err) {
            echo "cURL Error #:" . $err;
            die();
        }

        $response = json_decode($response, true);

        $score = $response['score_tag'];

        $this->storeSentiment($score);
    }

    public function storeSentiment($score){
        $tool = Tool::where('name', 'Meaning Cloud')->first();

        $sentiment = new Sentiment();
        $sentiment->tools_id = $tool->id;

        if ($score === 'N+') {
            $sentiment->label = 'Negative+';
            $sentiment->score = -1.0;
        }
        if ($score === 'N') {
            $sentiment->label = 'Negative';
            $sentiment->score = -0.5;
        }
        if ($score === 'NEU') {
            $sentiment->label = 'Neutral';
            $sentiment->score = 0;
        }
        if ($score === 'NONE') {
            $sentiment->label = 'Neutral';
            $sentiment->score = 0;
        }
        if ($score === 'P') {
            $sentiment->label = 'Positive';
            $sentiment->score = 0.5;
        }
        if ($score === 'P+') {
            $sentiment->label = 'Positive+';
            $sentiment->score = 1;
        }

        $sentiment->tweets_id = $this->tweet->id;
        $sentiment->save();
    }
}
