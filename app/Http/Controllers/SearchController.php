<?php

namespace App\Http\Controllers;

use App\Models\Emotion;
use App\Models\Keyword;
use App\Models\Sentiment;
use App\Models\Tool;
use App\Models\Tweet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Twitter;

class SearchController extends Controller
{
    public function search(){
        set_time_limit(0); // This sets the time limit of the program to 0, which allows it to run with no time limit.
        $keyword = Keyword::where('text', 'temer')->first(); // This will get the register of the keyword we are looking for
        $searchWord = $keyword->text; // This is the word that Twitter API will use to search related tweets to it.
        $maxId = $keyword['lastId']; // This will store the last id of the tweet received from twitter (higher == most recent)

        $query = [
                'q' => $searchWord,
                'count' => 100,
                'include_entities' => false,
                'result_type' => 'mixed',
                'lang' => 'pt',
                'until' => Carbon::now()->addDay()->format('Y-m-d')
        ];

        try{
            while(true) {
                DB::beginTransaction();

                $result = Twitter::getSearch($query);

                foreach ($result as $key){
                    if(isset($key)){
                        foreach ($key as $obj){
                            if (isset($obj->text)){

                                if(strpos($obj->text, $searchWord) !== false){
                                    $tweet = new Tweet();
                                    $tweet->text = $this->parseTweet($obj->text);
                                    $tweet->created_at = Carbon::parse($obj->created_at);
                                    $tweet->created_at = $tweet->created_at->format('d-m-Y H:i:s');
                                    $tweet->keywords_id = $keyword['id'];
                                    $tweet->save();

                                    $this->analyzeSentence($tweet);

                                }
                            }
                        }
                    }
                }

                $query = [
                    'q' => $searchWord,
                    'count' => 100,
                    'include_entities' => false,
                    'result_type' => 'mixed',
                    'since_id' => $maxId,
                    'lang' => 'pt',
                    'until' => Carbon::now()->addDay()->format('Y-m-d')
                ];
                DB::commit();
                time_nanosleep(60, 0);
            }
        }catch(\Exception $e){
            DB::rollBack();
            dd($e);
//            $this->search();
        }
    }

    /**
     * @param $tweet tweet that will be parsed to remove special chars and HTTP(S) links.
     * @return string|string[]|null the parsed string
     */
    protected function parseTweet($tweet) {

        $replace = '/\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i';

        $tweet = preg_replace($replace, '', $tweet); // This removes the https link that comes with a few tweets.
        $tweet = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),
            explode(" ","a A e E i I o O u U n N"),$tweet); // This removes any accent in the tweet.

        return $tweet;
    }

    public function analyzeSentence($tweet){

        $this->analyzeSentenceAzure($tweet);
//        $this->analyzeSentenceGotItAi($tweet);
        $this->analyzeSentenceMeaningCloud($tweet);
        $this->analyzeSentenceRepustate($tweet);

    }

    /**
     * @param $tweet tweet to be evaluated by the Azure API
     */
    protected function analyzeSentenceAzure($tweet){
        $azure = new Azure('4918c7c7f8354c3b8783b8bc0e4e3a62', $tweet);
        $azure->sentiment();
    }

    protected function analyzeSentenceGotItAi($tweet){
        $gotItAI = new GotItAI('245-lUo5KMnn','7YvUMtSD0WJ3ESF/stF2ofXx50WBsr2p7QT83skLoaCc', $tweet);
        $gotItAI->sentiment();
    }

    protected function analyzeSentenceRepustate($tweet){
        $repustate = new Repustate('da90c499d3923e92a4cb9234f027da97956979fd', $tweet);
        $repustate->sentiment('pt');
    }

    protected function analyzeSentenceMeaningCloud($tweet){
        $meaningCloud = new MeaningCloud('3a37f8070c6a2498ffa1154c283f6ad7', $tweet);
        $meaningCloud->sentiment();
    }

    public function getNetworkState($keywordsId){
        $data['sentiments'] = $this->getSentimentsState($keywordsId);
        return $data;
    }

    // TODO CHECK THIS FUNCTION
    protected function getSentimentsState($keywordsId){

        $count = 0;
        $data['neutral'] = 0;
        $data['positive'] = 0;
        $data['positive+'] = 0;
        $data['negative'] = 0;
        $data['negative+'] = 0;
        $data['positiveScore'] = 0;
        $data['negativeScore'] = 0;

        $tweets = Tweet::with('sentiment')->where('keywords_id', $keywordsId)->get();
        foreach ($tweets as $tweet) {

            switch ($tweet->sentiment->label){
                case "POSITIVE":
                    $data['positive'] ++;
                    $data['positiveScore'] += $tweet->sentiment->score;
                    break;

                case "NEGATIVE":
                    $data['negative'] ++;
                    $data['negativeScore'] += ($tweet->sentiment->score * -1);
                    break;

                default:
                    $data['neutral'] ++;
            }

            $count++;
        }

        $data['positiveScore'] = $data['positiveScore'] / $count * 100;
        $data['negativeScore'] = $data['negativeScore'] / $count * 100;

        return $data;
    }
}
