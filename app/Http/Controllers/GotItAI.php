<?php

namespace App\Http\Controllers;


use App\Models\Sentiment;
use App\Models\Tool;

class GotItAI
{
    public $key;
    public $secret;
    public $url = 'https://api.gotit.ai/NLU/Analyze';
    public $options;
    public $tweet;

    public function __construct($key, $secret, $tweet){

        $this->key = $key;
        $this->secret = $secret;
        $this->tweet = $tweet;

        $headers =  array(
            "Content-type: application/json",
            "Authorization: Basic ". base64_encode($this->key.":". $this->secret)
        );

        $data_array = array();
        $data_array ["T"] = $this->tweet->text;
        $data_array ["S"] = true; // Executa a feature de Sentimento
        $data = json_encode($data_array );

        $this->options = array (
            'http' => array (
                'header' => $headers,
                'method' => 'POST',
                'content' => $data
            )
        );
    }

    public function sentiment () {

        $context  = stream_context_create ($this->options);
        $result = file_get_contents ($this->url, false, $context);
        $result = json_decode($result, true);
        $result = $result['sentiment'];
        $score = $result['score'];
        $this->storeSentiment($score);
    }

    public function storeSentiment($score){
        $tool = Tool::where('name', 'Got it AI')->first();

        $sentiment = new Sentiment();
        $sentiment->tools_id = $tool->id;
        $sentiment->score = $score;

        if ($sentiment->score < -0.5) {
            $sentiment->label = 'Negative+';
        }
        if ($sentiment->score >= -0.5 && $sentiment->score < 0) {
            $sentiment->label = 'Negative';
        }
        if ($sentiment->score >= 0 && $sentiment->score < 0.1) {
            $sentiment->label = 'Neutral';
        }
        if ($sentiment->score >= 0.1 && $sentiment->score < 0.5) {
            $sentiment->label = 'Positive';
        }
        if ($sentiment->score >= 0.5) {
            $sentiment->label = 'Positive+';
        }

        $sentiment->tweets_id = $this->tweet->id;
        $sentiment->save();
    }

}
