<?php

namespace App\Http\Controllers;
//
//  Repustate PHP API client.
//
//  Requirements:
//  - PHP >= 5.3, with cURL and JSON
//
//  Want to change it / improve it / share it? Go for it.
//
//  Feedback is appreciated at info@repustate.com
//
//  More documentation available at http://www.repustate.com/docs
//

use App\Models\Sentiment;
use App\Models\Tool;

function subst($str, $dict){
    $foo =  function ($a) use ($str, $dict) {
        return "/%\\($a\\)s/";
    };
    return preg_replace(array_map($foo, array_keys($dict)), array_values($dict), $str);
}

class Repustate
{
    const urlTemplate = 'http://api.repustate.com/%(version)s/%(key)s/%(function)s.json';

    public $apiKey;
    public $version;
    public $tweet;

    public function __construct($apiKey, $tweet, $version='v3')
    {
        $this->apiKey = $apiKey;
        $this->version = $version;
        $this->tweet = $tweet;
    }

    //
    // Construct a query string from an array of arguments.
    //
    protected function getQueryString($args)
    {
        $data = array();
        foreach ($args as $k => $v)
        {
            // Skip empty values
            if ($v == '')
            {
                continue;
            }
            $data[] = $k . '=' . rawurlencode($v);
        }
        return join('&', $data);
    }

    //
    // Retrieve the url for the given api function.
    //
    protected function getUrlForCall($apiFunction)
    {
        if ($apiFunction == 'powerpoint')
        {
            $urlTemplate = self::pptUrl;
        }
        else
        {
            $urlTemplate = self::urlTemplate;
        }

        $urlArgs = array(
            'function' => $apiFunction,
            'key' => $this->apiKey,
            'version' => $this->version,
        );

        return subst($urlTemplate, $urlArgs);
    }

    /**
     * Make an HTTP request via cURL for the given url and data.
     */
    protected function getHttpResult($url, $data, $useHttpGet)
    {
        $ch = curl_init();

        if ($useHttpGet)
        {
            $url .= '?' . $data;
        }
        else
        {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, ! $useHttpGet);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $result = curl_exec($ch);

        if (curl_errno($ch))
        {
            throw new Exception('Curl Error: ' . curl_error($ch));
        }

        $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($httpStatus != 200)
        {
            throw new Exception('HTTP Error: ' . $httpStatus . ' ' . $result);
        }

        curl_close($ch);

        return $result;
    }

    protected function parseResult($result, $apiFunction)
    {
        // Default is JSON
        return json_decode($result, true);
    }

    protected function callApi($apiFunction, $args=null, $useHttpGet=FALSE)
    {
        $data = $this->getQueryString($args);
        $url = $this->getUrlForCall($apiFunction);
        $result = $this->getHttpResult($url, $data, $useHttpGet);
        return $this->parseResult($result, $apiFunction);
    }

    //
    // Retreive the sentiment for a single URL or block of text.
    //
    public function sentiment($lang='en')
    {
        $result = $this->callApi('score', array('text' => $this->tweet->text, 'lang' => $lang));
        $score = $result['score'];
        $this->storeSentiment($score);
    }

    public function storeSentiment($score){
        $tool = Tool::where('name', 'Repustate')->first();

        $sentiment = new Sentiment();
        $sentiment->tools_id = $tool->id;
        $sentiment->score = $score;

        if ($sentiment->score <= -0.5) {
            $sentiment->label = 'Negative+';
        }
        if ($sentiment->score > -0.5 && $sentiment->score < 0) {
            $sentiment->label = 'Negative';
        }
        if ($sentiment->score >= 0 && $sentiment->score < 0.1) {
            $sentiment->label = 'Neutral';
        }
        if ($sentiment->score >= 0.1 && $sentiment->score < 0.5) {
            $sentiment->label = 'Positive';
        }
        if ($sentiment->score >= 0.5) {
            $sentiment->label = 'Positive+';
        }

        $sentiment->tweets_id = $this->tweet->id;
        $sentiment->save();
    }

    //
    // Bulk score multiple pieces of text (not urls!).
    //
    public function bulkSentiment($items, $lang='en')
    {
        $itemsToScore = array();
        foreach (array_values($items) as $k => $v)
        {
            $itemsToScore['text' . $k] = $v;
        }
        $itemsToScore['lang'] = $lang;
        return $this->callApi('bulk-score', $itemsToScore);
    }
    
    //
    // Extract entities.
    //
    public function entities($text=null, $lang='en')
    {
        return $this->callApi('entities', array('text' => $text, 'lang' => $lang));
    }

    //
    // Clean up a web page. It doesn't work well on home pages - it's
    // designed for content pages.
    //
    public function clean_html($url=null)
    {
        if (is_array($url)) extract($url, EXTR_IF_EXISTS);
        return $this->callApi('clean-html', array('url' => $url),
                              $useHttpGet=TRUE);
    }
}

?>
