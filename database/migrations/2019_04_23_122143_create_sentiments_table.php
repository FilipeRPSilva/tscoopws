<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSentimentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tscoop.sentiments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label',20);
            $table->float('score');
            $table->unsignedInteger('tweets_id');
            $table->unsignedInteger('tools_id');

            $table->foreign('tweets_id')->references('id')->on('tscoop.tweets');
            $table->foreign('tools_id')->references('id')->on('tscoop.tools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tscoop.sentiments');
    }
}
