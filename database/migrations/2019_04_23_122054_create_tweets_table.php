<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTweetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tscoop.tweets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('text', 200);
            $table->dateTime('created_at');
            $table->unsignedInteger('keywords_id');

            $table->foreign('keywords_id')->references('id')->on('tscoop.keywords');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tscoop.tweets');
    }
}
